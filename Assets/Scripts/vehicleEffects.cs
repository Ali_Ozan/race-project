using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class vehicleEffects : MonoBehaviour
{
    private controller controller;
    private inputManager inputManager;

    public GameObject brakeLights;
    public AudioSource skidClip;

    public ParticleSystem[] smoke;
    public TrailRenderer[] tireMarks;
    public ParticleSystem[] nitros;

    private bool lightsFlag = false;
    private bool smokeFlag = false;
    private bool tireMarksFlag = false;

    void Start()
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;
        controller = GetComponent<controller>();
        inputManager = GetComponent<inputManager>();
        //brakeLights = GameObject.Find("BrakeLights").gameObject;
    }

    void FixedUpdate()
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;

        activateLights();
        activateSmoke();
        checkDrift();
    }

    private void activateLights()
    {
        if (inputManager.vertical < 0 || controller.KPH <= 1) turnLightsOn();
        else turnLightsOff();
    }

    private void turnLightsOn()
    {
        if (lightsFlag) return;
        brakeLights.SetActive(true);
        lightsFlag = true;
    }

    private void turnLightsOff()
    {
        if (!lightsFlag) return;
        brakeLights.SetActive(false);
        lightsFlag = false;
    }

    private void activateSmoke()
    {
        if (controller.playPauseSmoke) startSmoke();
        else stopSmoke();

        if (smokeFlag)
        {
            for (int i = 0; i < smoke.Length; i++)
            {
                var emission = smoke[i].emission;
                emission.rateOverTime = ((int)controller.KPH * 2 <= 200) ? (int)controller.KPH * 2 : 200;
            }
        }
    }

    public void startSmoke()
    {
        if (smokeFlag) return;
        for (int i = 0; i < smoke.Length; i++)
        {
            var emission = smoke[i].emission;
            emission.rateOverTime = ((int)controller.KPH * 2 <= 300) ? (int)controller.KPH * 2 : 200;
            smoke[i].Play();
        }
        smokeFlag = true;

    }

    public void stopSmoke()
    {
        if (!smokeFlag) return;
        for (int i = 0; i < smoke.Length; i++)
        {
            smoke[i].Stop();
        }
        smokeFlag = false;
    }

    private void checkDrift()
    {
        if ((controller.playPauseSmoke || (inputManager.handbrake && controller.KPH > 20)) && controller.isGrounded())
            startEmitter();
        else 
            stopEmitter();
    }

    private void startEmitter()
    {
        if (tireMarksFlag) 
            return;
        foreach (TrailRenderer T in tireMarks)
        {
            T.emitting = true;
        }
        skidClip.Play();
        tireMarksFlag = true;
    }
    private void stopEmitter()
    {
        if (!tireMarksFlag) 
            return;
        foreach (TrailRenderer T in tireMarks)
        {
            T.emitting = false;
        }
        skidClip.Stop();
        tireMarksFlag = false;
    }

    public void startNitroEmitter()
    {
        if (controller.nitrusFlag) return;
        for (int i = 0; i < nitros.Length; i++)
        {
            nitros[i].Play();
        }
        controller.nitrusFlag = true;
    }
    public void stopNitroEmitter()
    {
        if (!controller.nitrusFlag) return;
        for (int i = 0; i < nitros.Length; i++)
        {
            nitros[i].Stop();
        }
        controller.nitrusFlag = false;
    }

}
