using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class controller : MonoBehaviour
{
    private GameManager manager;
    private inputManager InputManager;
    private vehicleEffects VehicleEffects;

    private Rigidbody rigidbody;
    public GameObject wheelCollidersParent, wheelMeshesParent;
    public WheelCollider[] wheelColliders = new WheelCollider[4];
    public GameObject[] wheelMeshes = new GameObject[4];
    public GameObject centerOfMass;

    internal enum driveType
    {
        rearWheelDrive,
        frontWheelDrive,
        allWheelDrive
    }
    [SerializeField] private driveType wheelDrive;

    [Header("Variables")]
    public float maxRPM = 7000;
    public float minRPM = 3000;
    public float[] gears;
    public float[] gearChangeSpeed;
    public AnimationCurve engineTorque;
    public bool reverse;
    public float steeringMax = 30;
    private float initialSteeringRadius = 6;
    public float steeringRadius = 6;
    public float downForceValue = 70f;
    public float brakePower_base = 500;
    public float brakePower;
    public float boostThrust = 500f;

    [HideInInspector] public bool hasFinished = false;
    [HideInInspector] public int lapsCompleted = 0;
    private float smoothTime = 0.1f;

    public string carName;

    public float totalPower = 1200f;
    public float wheelsRPM;
    public float engineRPM;
    public float KPH;
    public int gearNum = 0;

    private WheelFrictionCurve forwardFriction, sidewaysFriction;
    private float handBrakeFrictionMultiplier = 0.7f;
    public float driftFactor;
    [HideInInspector] public bool playPauseSmoke;

    [HideInInspector] public float nitroValue;
    [HideInInspector] public bool nitrusFlag = false;

    [Header("DEBUG")]
    public float[] slip = new float[4];

    void Start()
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;
        getObjects();
        nitroValue = 5f;
    }
    private void getObjects()
    {
        InputManager = GetComponent<inputManager>();
        rigidbody = GetComponent<Rigidbody>();
        VehicleEffects = GetComponent<vehicleEffects>();
        manager = GameManager.FindObjectOfType<GameManager>();

        wheelCollidersParent = gameObject.transform.Find("wheelCollidersParent").gameObject; //doing this will save me from dragging and appointing the colliders&meshes everytime a new car copy is created or the object is reset. It will automatically find its own colliders and meshes by their name
        wheelColliders[0] = wheelCollidersParent.transform.Find("front_left").gameObject.GetComponent<WheelCollider>();
        wheelColliders[1] = wheelCollidersParent.transform.Find("front_right").gameObject.GetComponent<WheelCollider>();
        wheelColliders[2] = wheelCollidersParent.transform.Find("rear_left").gameObject.GetComponent<WheelCollider>();
        wheelColliders[3] = wheelCollidersParent.transform.Find("rear_right").gameObject.GetComponent<WheelCollider>();

        wheelMeshesParent = gameObject.transform.Find("wheelMeshesParent").gameObject;
        wheelMeshes[0] = wheelMeshesParent.transform.Find("front_left").gameObject;
        wheelMeshes[1] = wheelMeshesParent.transform.Find("front_right").gameObject;
        wheelMeshes[2] = wheelMeshesParent.transform.Find("rear_left").gameObject;
        wheelMeshes[3] = wheelMeshesParent.transform.Find("rear_right").gameObject;

        
        centerOfMass = GameObject.Find("centerOfMass");
        rigidbody.centerOfMass = centerOfMass.transform.localPosition;
    }

    void FixedUpdate()
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;
        animateWheels();
        moveVehicle();
        steerVehicle();
        addDownForce();
        getFriction();
        calculateEnginePower();
        shifter();
        StartCoroutine(timedLoop());
        if (gameObject.tag == "AI") return;
        adjustTraction();
        activateNitro();
    }

    private void calculateEnginePower()
    {
        wheelRPM();
        totalPower = engineTorque.Evaluate(engineRPM) * (gears[gearNum] * InputManager.vertical);
        engineRPM = Mathf.Lerp(engineRPM, 1000 + (Mathf.Abs(wheelsRPM) * 3.6f *  (gears[gearNum])), smoothTime);
        if (hasFinished)
            totalPower = 0f;
    }

    private void wheelRPM()
    {
        float sum = 0;
        for (int i = 0; i < 4; i++)
        {
            sum += wheelColliders[i].rpm;
        }
        wheelsRPM = sum / 4; //that's just a practical way to get a meaningful value to define the actually important engineRPM

        if (wheelsRPM < 0 && !reverse)
        {
            reverse = true;
            if (gameObject.tag != "AI") 
                manager.changeGear();
        }
        else if (wheelsRPM > 0 && reverse)
        {
            reverse = false;
            if (gameObject.tag != "AI") 
                manager.changeGear();
        }
    }

    private void shifter()
    {  //this is an automatic gearbox, a manual control can be added here if desired.
        if (reverse)
        {
            gearNum = 1;
        }
        else if ((engineRPM > maxRPM && gearNum < gears.Length - 1) && isGrounded())
        {
            gearNum++;
            if (gameObject.tag != "AI")
                manager.changeGear();
        }
        else if ((engineRPM < minRPM && gearNum > 0) && isGrounded())
        {
            gearNum--;
            if (gameObject.tag != "AI")
                manager.changeGear();
        }
    }

    public bool isGrounded() //cheks if all wheels touch the ground (useful for keeping the gear while the car flies)
    {
        if (wheelColliders[0].isGrounded && wheelColliders[1].isGrounded && wheelColliders[2].isGrounded && wheelColliders[3].isGrounded)
            return true;
        else
            return false;
    }

    private void moveVehicle()
    {
        brakeVehicle();

        if (wheelDrive == driveType.allWheelDrive)
        {
            for (int i = 0; i < wheelColliders.Length; i++)
                wheelColliders[i].motorTorque = Mathf.Abs(totalPower) / 4;
        }
        else if (wheelDrive == driveType.frontWheelDrive)
        {
            for (int i = 0; i < wheelColliders.Length - 2; i++)
                wheelColliders[i].motorTorque = Mathf.Abs(totalPower) / 2;
        }
        else
        {
            for (int i = 2; i < wheelColliders.Length; i++)
                wheelColliders[i].motorTorque = InputManager.vertical * (Mathf.Abs(totalPower) / 2);
        }

        KPH = rigidbody.velocity.magnitude * 3.6f; //conversion from m/s to KPH


        for (int i = 0; i < wheelColliders.Length; i++)
            wheelColliders[i].brakeTorque = brakePower;

        if (InputManager.handbrake) //want to drift
            wheelColliders[2].brakeTorque = wheelColliders[3].brakeTorque = brakePower_base / 5;
    }

    private void brakeVehicle()
    {
        if ((InputManager.vertical < 0 && !reverse && KPH >= 10) || hasFinished)
            brakePower = brakePower_base;
        else if (InputManager.vertical == 0 && (KPH <= 10 || KPH >= -10))
            brakePower = 10;
        else
            brakePower = 0;
    }

    private void steerVehicle()
    {
        //Ackermann steering formula 

        if (InputManager.horizontal > 0)
        {
            wheelColliders[0].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (steeringRadius + (1.5f / 2))) * InputManager.horizontal;
            wheelColliders[1].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (steeringRadius + (1.5f / 2))) * InputManager.horizontal;
        }
        else if (InputManager.horizontal < 0)
        {
            wheelColliders[0].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (steeringRadius + (1.5f / 2))) * InputManager.horizontal;
            wheelColliders[1].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (steeringRadius + (1.5f / 2))) * InputManager.horizontal;
        }
        else
        {
            wheelColliders[0].steerAngle = 0;
            wheelColliders[0].steerAngle = 0;
        }
    }

    private void animateWheels()
    {
        Vector3 wheelPosition = Vector3.zero;
        Quaternion wheelRotation = Quaternion.identity;

        for (int i = 0; i < 4; i++)
        {
            wheelColliders[i].GetWorldPose(out wheelPosition, out wheelRotation);
            wheelMeshes[i].transform.position = wheelPosition;
            wheelMeshes[i].transform.rotation = wheelRotation;
        }
    }
    private void addDownForce()
    {
        rigidbody.AddForce(-transform.up * downForceValue * rigidbody.velocity.magnitude);
    }
    private void getFriction()
    {
        for (int i = 0; i < wheelColliders.Length; i++)
        {
            WheelHit wheelHit;
            wheelColliders[i].GetGroundHit(out wheelHit);

            slip[i] = wheelHit.forwardSlip; //returns the friction loss if we go sideways/drift
        }
    }

    private void adjustTraction()
    {
        sidewaysFriction = wheelColliders[0].sidewaysFriction;
        forwardFriction = wheelColliders[0].forwardFriction;

        if (InputManager.handbrake) //drift
        {
            sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue = forwardFriction.extremumValue = forwardFriction.asymptoteValue =
                Mathf.Lerp(forwardFriction.asymptoteValue, driftFactor * handBrakeFrictionMultiplier, .5f * Time.deltaTime);

            wheelColliders[2].sidewaysFriction = sidewaysFriction;
            wheelColliders[3].forwardFriction = forwardFriction;
            
            //extra grip for the front wheels
            sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue = forwardFriction.extremumValue = forwardFriction.asymptoteValue = 3f;
            wheelColliders[0].sidewaysFriction = sidewaysFriction;
            wheelColliders[1].forwardFriction = forwardFriction;

            //rigidbody.AddForce(transform.forward * (KPH / 10) * totalPower); //so that while drifting the car doesnot slow significantly and drift with a better feeling
        }
        //executed when handbrake is being held
        else
        {
            forwardFriction.extremumValue = forwardFriction.asymptoteValue = sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue =
                ((KPH * handBrakeFrictionMultiplier) / 300) + 1;

            for (int i = 0; i < 4; i++)
            {
                wheelColliders[i].forwardFriction = forwardFriction;
                wheelColliders[i].sidewaysFriction = sidewaysFriction;
            }
        }

        //checks the amount of slip to control the drift
        for (int i = 2; i < 4; i++)
        {
            WheelHit wheelHit;

            wheelColliders[i].GetGroundHit(out wheelHit);
            //smoke
            if (wheelHit.sidewaysSlip >= 0.3f || wheelHit.sidewaysSlip <= -0.3f || wheelHit.forwardSlip >= .3f || wheelHit.forwardSlip <= -0.3f)
                playPauseSmoke = true;
            else
                playPauseSmoke = false;


            if (wheelHit.sidewaysSlip < 0) driftFactor = (1 + -InputManager.horizontal) * Mathf.Abs(wheelHit.sidewaysSlip);

            if (wheelHit.sidewaysSlip > 0) driftFactor = (1 + InputManager.horizontal) * Mathf.Abs(wheelHit.sidewaysSlip);
        }
    }

    private IEnumerator timedLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(.7f);
            steeringRadius = initialSteeringRadius + KPH / 40;
        }
    }

    public void activateNitro()
    {
        if (!InputManager.boosting && nitroValue <= 10)
        {
            if (playPauseSmoke)
                nitroValue += Time.deltaTime / 2; //drifting fills the nitro faster
            else
                nitroValue += Time.deltaTime / 4;
        }
        else
        {
            nitroValue -= (nitroValue <= 0) ? 0 : Time.deltaTime; 
        }

        if (InputManager.boosting)
        {
            if (nitroValue > 0)
            {
                VehicleEffects.startNitroEmitter();
                rigidbody.AddForce(rigidbody.transform.forward * boostThrust);
            }
            else VehicleEffects.stopNitroEmitter();
        }
        else VehicleEffects.stopNitroEmitter();
    }
}
