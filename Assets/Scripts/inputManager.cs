﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class inputManager : MonoBehaviour 
{
    internal enum driver { AI, keyboard};
    [SerializeField] internal driver driverController;

    public float vertical;
    public float horizontal;
    public bool handbrake;
    public bool boosting;

    public trackWayPoints wayPoints;
    private Vector3 currentWaypoint;
    public List<Transform> nodes = new List<Transform>();
    public int currentNode;

    private Vector3 rotationAtLastNode = Vector3.zero;
    private int nodeAtLastCheck = 1;


    [Range(1, 10)] public int distanceOffset; //how many nodes forward the AI will aim
    [Range(0, 5)] public float AISteerIntensity;
    [Range(0.3f, 1f)] public float AIDifficulty;

    [HideInInspector] public bool wentHalfwayThrough = false;  //will be used to legitimize completing a lap during collision with finish trigger

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;
        wayPoints = GameObject.FindGameObjectWithTag("path").GetComponent<trackWayPoints>();
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;
        nodes = wayPoints.trackNodes;
        InvokeRepeating("checkIfStuck", 10f, 6f);
    }
    private void FixedUpdate() 
    {
        if (SceneManager.GetActiveScene().name == "OpeningScene")
            return;
        calculateDistanceOfWayPoints();

        switch (driverController)
        {
            case driver.AI:
            {
                AIDrive();
                break;
            }
            case driver.keyboard:
            {
                keyboardDrive();
                break;
            }
        }
    }

    private void AIDrive()
    {
        vertical = AIDifficulty; 
        AISteer();
    }
    private void keyboardDrive()
    {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
        handbrake = Input.GetAxis("Jump") == 0 ? false : true;
        if (Input.GetKey(KeyCode.LeftShift)) boosting = true; else boosting = false;
    }

    private void calculateDistanceOfWayPoints()
    {
        Vector3 vehiclePosition = gameObject.transform.position;
        float distance = Mathf.Infinity;

        for (int i = 0; i < nodes.Count; i++)
        {
            float currentDistance = (nodes[i].transform.position - vehiclePosition).magnitude;
            if (currentDistance < distance)
            {
                if ((i + distanceOffset) >= nodes.Count) //lap completion
                    currentWaypoint = nodes[i + distanceOffset - nodes.Count].transform.position;
                else
                    currentWaypoint = nodes[i + distanceOffset].transform.position;

                distance = currentDistance;
                currentNode = i;
            }
        }

        if (!wentHalfwayThrough && currentNode == (int)nodes.Count / 2)
            wentHalfwayThrough = true;
        if (wentHalfwayThrough && currentNode == nodes.Count) 
            wentHalfwayThrough = false;
    }

    private void AISteer()
    {
        Vector3 relative = transform.InverseTransformPoint(currentWaypoint);
        relative /= relative.magnitude;

        horizontal = (relative.x / relative.magnitude) * AISteerIntensity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(currentWaypoint, 3);
    }

    void checkIfStuck()
    {
        if (nodeAtLastCheck == currentNode && vertical > 0.01f)
        {
            transform.position = currentWaypoint;
            transform.eulerAngles = new Vector3(0, rotationAtLastNode.y, 0);
        }   
        else
        {
           nodeAtLastCheck = currentNode;
           rotationAtLastNode = transform.eulerAngles;
        }
    }
}