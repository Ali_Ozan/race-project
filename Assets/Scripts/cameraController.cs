﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour {

    private GameObject Player;
    private GameObject CameraConstraint;
    private controller controller;
    public float LerpSpeed = 30;
    public float defaltFOV = 0, desiredFOV = 0;
    [Range (0, 5)] public float cameraFOV_lerpTimeMultiplier = 2;

    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        CameraConstraint = Player.transform.Find("camera constraint").gameObject;
        controller = Player.GetComponent<controller>();
        defaltFOV = Camera.main.fieldOfView;
        desiredFOV = defaltFOV + 20; //this may change according to desire
    }


    private void FixedUpdate()
    {
        follow();
        boostFOV();
    }


    private void follow()
    {
        LerpSpeed = Mathf.Lerp(LerpSpeed, controller.KPH/4, Time.deltaTime);

        Vector3 targetPosition = new Vector3(CameraConstraint.transform.position.x, CameraConstraint.transform.position.y, CameraConstraint.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * LerpSpeed);
        transform.LookAt(Player.gameObject.transform.position);
    }

    private void boostFOV()
    {
        if (controller.nitrusFlag)
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, desiredFOV, Time.deltaTime * cameraFOV_lerpTimeMultiplier);
        }
        else //if we're not boosting, lerp back to default
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, defaltFOV, Time.deltaTime * cameraFOV_lerpTimeMultiplier);
    }

}