using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finishTrigger : MonoBehaviour
{
    public controller Controller;
    public inputManager inputManager;
    public GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish" && inputManager.wentHalfwayThrough)
        {
            Controller.lapsCompleted++;
            if (Controller.lapsCompleted == gameManager.totalLapCount)
                Controller.hasFinished = true;
        }
    }
}
