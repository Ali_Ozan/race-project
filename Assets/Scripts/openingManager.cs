using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class openingManager : MonoBehaviour
{
    public GameObject toRotate;
    public GameObject player;
    public float rotateSpeed = 12;

    public vehicleList listOfVehicles;
    public int vehicleIndex = 0;

    private void Awake()
    {
        vehicleIndex = PlayerPrefs.GetInt("vehicleIndex");
        //vehicleIndex = 0;

        GameObject childObject = Instantiate(listOfVehicles.Vehicles[vehicleIndex], Vector3.zero, Quaternion.identity);
        childObject.transform.parent = toRotate.transform;
    }

    private void FixedUpdate()
    {
        toRotate.transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
    }

    public void rightButton()
    {
        if (vehicleIndex < listOfVehicles.Vehicles.Length - 1)
        {
            vehicleIndex++;
            instantiateVehicle();
        }
    }

    public void leftButton()
    {
        if (vehicleIndex > 0)
        {
            vehicleIndex--;
            instantiateVehicle();
        }
    }

    private void instantiateVehicle()
    {
        Quaternion lastRotation = GameObject.FindGameObjectWithTag("Player").transform.rotation;
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        PlayerPrefs.SetInt("vehicleIndex", vehicleIndex);
        GameObject childObject = Instantiate(listOfVehicles.Vehicles[vehicleIndex],Vector3.zero, lastRotation);
        childObject.GetComponent<Rigidbody>().isKinematic = true; // to keep the cars in perfect steadiness when switching
        childObject.transform.parent = toRotate.transform; //instantiated vehicle will rotate with the turntable this way
    }

    public void startGame()
    {
        SceneManager.LoadScene("MarioKartWorld");
    }
}
