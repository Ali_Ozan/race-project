using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trackWayPoints : MonoBehaviour
{
    public List<Transform> trackNodes = new List<Transform>();
    public Color trackLineGizmoColor;
    [Range(0, 1)] public float nodeSphereRadius;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = trackLineGizmoColor;
        Transform[] path = GetComponentsInChildren<Transform>();

        trackNodes = new List<Transform>();
        for (int i = 1; i < path.Length; i++) //starting from one to avoid adding the parent too to the list 
        {
            trackNodes.Add(path[i]);
        }

        for (int i = 0; i < trackNodes.Count; i++)
        {
            Vector3 currentWayPoint = trackNodes[i].position;
            Vector3 previousWayPoint = Vector3.zero;

            if (i != 0)
                previousWayPoint = trackNodes[i - 1].position;
            else if (i == 0)
                previousWayPoint = trackNodes[trackNodes.Count - 1].position; //first point will be connected to the last node

            Gizmos.DrawLine(previousWayPoint, currentWayPoint);
            Gizmos.DrawSphere(currentWayPoint, nodeSphereRadius);
        }
    }
}
