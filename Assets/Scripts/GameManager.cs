using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public controller controller;
    public inputManager inputManager;
    public vehicleList vehiclesList;
    public GameObject startPosition;

    public GameObject speedometerNeedle;
    public GameObject RPMNeedle;
    private float speedometerNeedle_startPos = -140f;
    private float speedometerNeedle_endPos = -43f;
    private float RPMNeedle_startPos = 23f;
    private float RPMNeedle_endPos = -203f;
    public float anglesPerRPM;
    public float vehicleSpeed;
    public Slider nitrusSlider;

    public Text currentPosition;
    private int playerPosition;

    [Header("countdown Timer")]
    public float timeLeft = 3.9f;
    public Text timeLeftText;

    [Range(1, 6)] public int totalLapCount;

    private bool countdownFlag = false;

    public Text speedText;
    public Text gearText;
    public Text lapText;
    public Text finishText;

    public GameObject[] presentVehiclesObjectsArray;
    public GameObject[] fullArray;
    private List<vehicle> presentVehicles = new List<vehicle>();
    private List<GameObject> temporaryList;


    private void Awake()
    {
        Instantiate(vehiclesList.Vehicles[PlayerPrefs.GetInt("vehicleIndex")], startPosition.transform.position, startPosition.transform.rotation);
        controller = GameObject.FindGameObjectWithTag("Player").GetComponent<controller>();
        inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<inputManager>();
        inputManager.driverController = inputManager.driver.keyboard;
        presentVehiclesObjectsArray = GameObject.FindGameObjectsWithTag("AI");
        foreach (GameObject O in presentVehiclesObjectsArray)
            presentVehicles.Add(new vehicle(O.GetComponent<inputManager>().currentNode, O.GetComponent<controller>().carName, O.GetComponent<controller>().lapsCompleted,  O.GetComponent<controller>().hasFinished));  //adding opponents to the list
        presentVehicles.Add(new vehicle(controller.gameObject.GetComponent<inputManager>().currentNode, controller.carName, controller.lapsCompleted, controller.hasFinished)); //adding the player to the list

        temporaryList = new List<GameObject>();
        foreach (GameObject R in presentVehiclesObjectsArray)
            temporaryList.Add(R);
        temporaryList.Add(controller.gameObject);

        fullArray = temporaryList.ToArray();

        StartCoroutine(timedLoop());
    }

    void FixedUpdate()
    {
        vehicleSpeed = controller.KPH;
        speedText.text = vehicleSpeed.ToString("0");
        updateSpeedometerNeedle();
        updateRPMneedle();
        coundDownTimer();
        lapCounter();
        updateNitro();
        updateFinishText();
    }

    private void updateFinishText()
    {
        if (controller.hasFinished)
        {
            if (playerPosition == 1)
                finishText.text = "YOU WON";
            else
                finishText.text = "YOU HAVE FINISHED " + currentPosition.text;
        }
    }

    private void updateNitro()
    {
        nitrusSlider.value = 0.64f * controller.nitroValue / 10f;
    }

    private void updatePlayerPosition()
    {
        if (!controller.hasFinished)
        {
            playerPosition = presentVehicles.Count;

            for (int i = 0; i < presentVehicles.Count; i++)
            {
                if ((controller.lapsCompleted * 9999 + controller.gameObject.GetComponent<inputManager>().currentNode) > (presentVehicles[i].lapsCompleted * 9999 + presentVehicles[i].node))
                {
                    playerPosition--;
                }
            }

            if (playerPosition == 1 || playerPosition == 11)
                currentPosition.text = (playerPosition).ToString() + "st";
            else if (playerPosition == 2)
                currentPosition.text = (playerPosition).ToString() + "nd";
            else if (playerPosition == 3)
                currentPosition.text = (playerPosition).ToString() + "rd";
            else
                currentPosition.text = (playerPosition).ToString() + "th";
        }
        else
        {

        }
    }

    private void lapCounter()
    {
        if (!controller.hasFinished)
            lapText.text = "LAP" + (controller.lapsCompleted + 1).ToString();
    }

    private void updateRPMneedle()
    {
        anglesPerRPM = Mathf.Abs(RPMNeedle_endPos - RPMNeedle_startPos) / 10000;//the gauge has 0 to 10000rpm
        RPMNeedle.transform.eulerAngles = new Vector3(0, 0, (RPMNeedle_startPos - anglesPerRPM * controller.engineRPM));
    }

    void updateSpeedometerNeedle()
    {
        float anglesPerKPH = (360 - Mathf.Abs(speedometerNeedle_endPos - speedometerNeedle_startPos)) / 180;//the gauge has 0 to 180 kph
        speedometerNeedle.transform.eulerAngles = new Vector3(0, 0, (speedometerNeedle_startPos - anglesPerKPH * vehicleSpeed));
    }

    public void changeGear()
    {
        if (controller.reverse)
            gearText.text = "R";
        else
            gearText.text = (controller.gearNum + 1).ToString();
    }

    private IEnumerator timedLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(.5f);
            sortArray();
        }
    }

    private void sortArray()
    {
        //NOTE: this function actually serves no purpose for now. This might be useful to have a table shown at the end of the race(or maybe even when the race is on) with all the racers listed.
        for (int i = 0; i < presentVehicles.Count; i++)
        {
            presentVehicles[i].hasFinished = fullArray[i].GetComponent<controller>().hasFinished;
            presentVehicles[i].lapsCompleted = fullArray[i].GetComponent<controller>().lapsCompleted;
            presentVehicles[i].name = fullArray[i].GetComponent<controller>().carName;
            presentVehicles[i].node = fullArray[i].GetComponent<inputManager>().currentNode;
        }

        if (!controller.hasFinished)
        {
            for (int i = 0; i < presentVehicles.Count; i++)  //bubble sort 
            {
                for (int j = i + 1; j < presentVehicles.Count; j++)
                {
                    if ((presentVehicles[j].lapsCompleted * 9999 + presentVehicles[j].node) > (presentVehicles[i].lapsCompleted * 9999 + presentVehicles[i].node)) //multiplying laps with a really large number so that a vehicle that is a lap behind cant rank higher that it actually is.
                    {
                        vehicle QQ = presentVehicles[i];
                        presentVehicles[i] = presentVehicles[j];
                        presentVehicles[j] = QQ;
                    }
                }
            }
        }

        updatePlayerPosition();
    }

    private void coundDownTimer()
    {
        if (timeLeft <= -5) return;
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0) unfreezePlayers();
        else freezePlayers();

        if (timeLeft > 1) timeLeftText.text = timeLeft.ToString("0");
        else if (timeLeft >= 0 && timeLeft <= 1) timeLeftText.text = "GO!";
        else timeLeftText.text = "";
    }

    private void freezePlayers()
    {
        if (countdownFlag) return;
        foreach (GameObject D in fullArray)
        {
            D.GetComponent<Rigidbody>().isKinematic = true;
        }
        countdownFlag = true;
    }

    private void unfreezePlayers()
    {
        if (!countdownFlag) return;
        foreach (GameObject D in fullArray)
        {
            D.GetComponent<Rigidbody>().isKinematic = false;
        }
        countdownFlag = false;
    }
}
