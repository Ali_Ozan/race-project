using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vehicle
{
    public int node;
    public string name;
    public int lapsCompleted;
    public bool hasFinished;

    public vehicle(int node, string name, int LapsCompleted, bool HasFinished)
    {
        this.node = node;
        this.name = name;
        lapsCompleted = LapsCompleted;
        hasFinished = HasFinished;
    }
}
